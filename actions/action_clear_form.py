from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals

from rasa_sdk import Action
from rasa_sdk.events import SlotSet, FollowupAction, Form
import json
import os
from lib.db_queries import *
from lib.resume import *

class ActionClearForm(Action):
	def name(self):
		return 'action_clear_form'
		
	def run(self, dispatcher, tracker, domain):
		try:
			return [SlotSet("requested_slot",None),Form(None)]
		except BaseException as e:
			print(e)
			ssml = "<s>I am sorry! I am still learning and I could not understand what you said. Could you please say that again?</s>"
			fallback_message = '[{ "message": { "template": { "elements": { "title": "I am sorry! I am still learning and I could not understand what you said. Could you please say that again?", "says": "", "visemes": ""},"type": "Card"} } },{"ssml": "<speak>' + ssml + '</speak>"}]'
			dispatcher.utter_message(fallback_message)
			return []