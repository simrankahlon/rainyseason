import os
from env import *
## Command to run core server
server_command = "export AWS_DEFAULT_REGION="+ AWS_DEFAULT_REGION +" && export BUCKET_NAME="+ BUCKET_NAME + " && export AWS_ENDPOINT_URL="+ AWS_ENDPOINT_URL +" && python3 -m rasa run -m" + MODEL_NAME + " --endpoints endpoints.yml -p 5002 --enable-api --credentials credentials.yml --debug --remote-storage aws --debug --cors \"*\""
os.system(server_command)
